# DCST1005

Dette prosjektet setter opp en AD-struktur med GPOer slik at ansatte i en bedrift får kun de tilgangene de bør ha.

## Hvordan kjøre skriptene

Du finner informasjon om innlogging i PDF-en vår

Powershell må alltid åpnes som administrator for alle skript

1. Kjør <code>iwr -useb https://gitlab.stud.idi.ntnu.no/kornelg/dcst1005/-/raw/main/ferdige_script/felles.ps1 | iex</code> på dc1
2. Lukk powershell og åpne igjen
3. kjør <code>cd C:\;git clone https://gitlab.stud.idi.ntnu.no/kornelg/dcst1005.git</code> på dc1
4. kjør dc1-1.ps1 på dc1
5. kjør <code>iwr -useb https://gitlab.stud.idi.ntnu.no/kornelg/dcst1005/-/raw/main/ferdige_script/felles.ps1 | iex</code> på alle maskiner untatt dc1
6. kjør dc1-2.ps1 på dc1
7. LEGG til cl1 i rett avdeling på OU=Computers,OU=Spera,DC=spera,DC=sec i AD users and computers på dc1
8. kjør <code>cd C:\;git clone https://gitlab.stud.idi.ntnu.no/kornelg/dcst1005.git</code> på alle maskiner untatt dc1
9. kjør <code>.\ferdige_script\srv1.ps1</code> på srv1
10. Map en disk som skal brukes til backup på G:\ på srv1
11. kjør <code>.\ferdige_script\setup-backup.ps1</code> på srv1

12. Fra nå av kan du bruke mgr1 og koble til eksternt med <code>Enter-PSSession [hostname]</code>, med bruker spera.sec\administrator
13. Du må nå bruke <code>cd C:\dcst1005</code> for å navigere til riktig working-directory
14. kjør <code>.\ferdige_script\create-gpo.ps1<code> på dc1
15. kjør <code>.\ferdige_script\new-ADUser.ps1</code> på dc1
16. Kjør <code>.\ferdige_script\new-ADusersIT.ps1</code> på dc1
17. Sett opp backup.ps1 i Task Scheduler på srv1
18. Følg GPO.txt for å legge til riktig Group Policy på riktig GPO på dc1
19. Bilder og forklaring finnes i PDF
20. Sett opp monitorering i Task Scheduler på dc1
21. Sett opp Overvåking-kritisk.ps1 på dc1
