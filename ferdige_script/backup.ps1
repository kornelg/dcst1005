Set-Disk -Number 1 -IsOffline $False # Online
diskpart -s readonly-disk

$date = Get-date -UFormat "%Y%m%d" #To name the folder
$weekdate = Get-Date -UFormat "%V"
$checkday = Get-date -UFormat "%A" # check witch day it is to dermine if full or incremental backup should run
$fullbackup = "-full"
$incrementalbackup = "-incremental"
$source = '\\spera.sec\files\'
$destination = 'G:\logs\'

#checks if destination exists, creates folder and runs backup if the folder does not exsist 
$checkdestfull = -join ($destination, $date, $fullbackup) #Merges G:\ and the date
$checkdestinc = -join ($destination, $weekdate, $incrementalbackup) #Merges G:\ and the date

#Checks if the folder G:\<todays date> exists, if it does NOT exist and today day is Sunday, the foldes is created and full backup is taken
#If the folder exists, the backup is expected to be taken and the script does nothing.
if (-not(Test-Path -Path $checkdestfull -PathType Container) -and ($checkday -eq "Sunday")) {
    New-Item -Path $checkdestfull -ItemType Directory

    Robocopy $source $checkdestfull /e /z /r:3 /w:10 /copy:DAT
}
else {
    #Checks if the folder G:\<todays date> exists, if it does NOT exist and today day is not Sunday, the foldes is created 
    #If the folder exists, the backup is expected to be taken and the script does nothing.
    New-Item -Path $checkdestinc -ItemType Directory
    Robocopy $source $checkdestinc /e /z /im /r:3 /w:10 /copy:DAT 
}

Set-Disk -Number 1 -IsOffline $True # Offline