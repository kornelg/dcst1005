$languagelist = Get-WinUserLanguageList
$LanguageList.Add("nb")
Write-Output "Changing List of Languages"
Set-WinUserLanguageList $languagelist

Set-TimeZone -id 'Central Europe Standard Time'

Set-ExecutionPolicy -ExecutionPolicy unrestricted -Scope LocalMachine

#Downloads chocolatey
Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
choco upgrade chocolatey
choco install -y git
#skal vi installere powershell og sysinternals?
#choco install -y powershell-core
#choco install -y sysinternals ?

#melder pc'ene inn i AD
$name = hostname
if ($name -ne 'dc1') {   
    $IP = Read-Host -Prompt 'Enter local 192.x.x.x IP of dc1'
    Get-NetAdapter | Set-DnsClientServerAddress -ServerAddresses $IP
    
    $cred = Get-Credential -UserName 'spera\Administrator' -Message 'Cred'
    Add-Computer -Credential $cred -DomainName spera.sec -PassThru -Verbose
    Write-Output "This machine will now restart"
    Pause
    Restart-Computer
} 