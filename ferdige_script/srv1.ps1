#Run this on srv1

# Installing IIS
Install-WindowsFeature -Name Web-Server

# Installing DFS
Install-WindowsFeature -Name FS-DFS-Namespace,FS-DFS-Replication,RSAT-DFS-Mgmt-Con -IncludeManagementTools
#Import-module dfsn
$folders = ('C:\dfsroots\files','C:\shares\IT','C:\shares\dev','C:\shares\HR','C:\shares\finance','C:\shares\sales')
mkdir -path $folders
$folders | ForEach-Object {$sharename = (Get-Item $_).name; New-SMBShare -Name $shareName -Path $_ -FullAccess Everyone}
New-DfsnRoot -TargetPath \\srv1\files -Path \\spera.sec\files -Type DomainV2
$folders | Where-Object {$_ -like "*shares*"} | ForEach-Object {$name = (Get-Item $_).name; $DfsPath = ('\\spera.sec\files\' + $name); $targetPath = ('\\srv1\' + $name);New-DfsnFolderTarget -Path $dfsPath -TargetPath $targetPath}
Get-ChildItem \\spera\files

<# 
    ContainerInherit - Forplant til underordnede beholdere (kataloger).
    None - Ikke spre denne tillatelsen til noen underordnede beholdere eller objekter.
    ObjectInherit - Forplante seg til underordnede objekter (filer).

    InheritOnly - Spesifiserer at ACE-en bare spres til underordnede objekter.
    None - No inheritance flagg satt.
    NoPropagateInherit - ACE går ikke nedover til underelementer.
#>

function createDFS {
    param (
        $Department
    )
    $ACL = Get-Acl "\\spera.sec\files\$Department"
    $folders = "\\spera.sec\files\$Department"
    Get-SmbShareAccess -name $Department
    Get-Acl -Path $folders
    (Get-Acl -Path $folders).Access
    (Get-Acl -Path $folders).Access | Format-Table -AutoSize
    (Get-Acl -Path $folders).Access | Where-Object {$_.IsInherited -eq $true} | Format-Table -AutoSize
    (Get-ACL -Path $folders).Access | Format-Table IdentityReference,FileSystemRights,AccessControlType,IsInherited,InheritanceFlags -AutoSize

    # TODO: test if we can use Get-Acl only once
    $folders = ('C:\shares\$Department')

    $ACL = Get-Acl \\spera.sec\files\$Department
    $AccessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("spera\l_$Department","FullControl",3,0,"Allow")
    $ACL.SetAccessRule($AccessRule)
    $ACL | Set-Acl -Path "\\spera.sec\files\$Department"

    # SetAccessRuleProtection
    $ACL = Get-Acl -Path "\\spera.sec\files\$Department"
    $ACL.SetAccessRuleProtection($true,$true)
    $ACL | Set-Acl -Path "\\spera.sec\files\$Department"

    $ACL = Get-Acl "\\spera.sec\files\$Department"
    $ACL.Access | Where-Object {$_.IdentityReference -eq "BUILTIN\Users" } | ForEach-Object { $ACL.RemoveAccessRuleSpecific($_) }
    Set-Acl "\\spera.sec\files\$Department" $ACL
    (Get-ACL -Path "\\spera.sec\files\$Department").Access | Format-Table IdentityReference,FileSystemRights,AccessControlType,IsInherited,InheritanceFlags -AutoSize
}

createDFS('sales')
createDFS('dev')
createDFS('HR')
createDFS('IT')
createDFS('finance')