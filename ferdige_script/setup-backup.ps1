Initialize-Disk 1

#Add volume to the VM through Openstack first 
New-Partition -disknumber 1 -usemaximumsize | Format-Volume -filesystem NTFS -newfilesystemlabel Backup 
#Create drive letters 
Get-Partition -disknumber 1 | Set-Partition -newdriveletter G

#Creates folder logs
mkdir G:\logs