#Creates OU structure in AD
New-ADOrganizationalUnit "Spera" -Description "Spera OU"
New-ADOrganizationalUnit "Employees" -Path "OU=Spera,DC=spera,DC=sec" -Description "Employees OU"
New-ADOrganizationalUnit "Computers" -Path "OU=Spera,DC=spera,DC=sec" -Description "Computers OU"
New-ADOrganizationalUnit "Groups" -Path "OU=Spera,DC=spera,DC=sec" -Description "Groups OU"


#Creates departments in the company, also used to create groups in the departments
$department = "dev", "finance", "sales", "IT", "HR"
foreach ($name in $department) { 
    New-ADOrganizationalUnit "$name" -Path "OU=Employees,OU=Spera,DC=spera,DC=sec" -Description "Employees/$name OU" 
    New-ADOrganizationalUnit "$name" -Path "OU=Computers,OU=Spera,DC=spera,DC=sec" -Description "Computers/$name OU" 
}
$groups = "local", "global"
foreach ($name in $groups) { 
    New-ADOrganizationalUnit "$name" -Path "OU=Groups,OU=Spera,DC=spera,DC=sec" -Description "Groups/$name OU"
}

$localgroups = @('l_it', 'l_dev', 'l_finance', 'l_sales', 'l_hr', 'l_remoteaccess')
$localgroups | ForEach-Object { New-ADGroup -GroupCategory Security `
        -GroupScope DomainLocal `
        -Name $_ `
        -Path "OU=local,OU=Groups,OU=Spera,DC=spera,DC=sec" `
        -SamAccountName "$_" }

$globalgroups = @('g_it', 'g_dev', 'g_finance', 'g_sales', 'g_hr')
$globalgroups | ForEach-Object { New-ADGroup -GroupCategory Security `
        -GroupScope Global `
        -Name $_ `
        -Path "OU=global,OU=Groups,OU=Spera,DC=spera,DC=sec" `
        -SamAccountName "$_"

    $department | ForEach-Object {
        $localgroups = Get-ADgroup -filter * | Where-object name -eq "l_$_"      
        $localgroups | Select-Object name, samaccountname            
        $globalgroup = Get-ADGroup -filter * | Where-object name -eq "g_$_"   
        $globalgroup | Select-Object name, samaccountname
        Write-Host $localgroups
        Write-Host $globalgroup         
        $localgroups | Add-ADGroupMember -Members $globalgroup.samaccountname 
    }
}

$printgroups = @('print_it', 'print_dev', 'print_finance', 'print_sales', 'print_hr')
$printgroups | ForEach-Object { New-ADGroup -GroupCategory Security `
        -GroupScope DomainLocal `
        -Name $_ `
        -Path "OU=local,OU=Groups,OU=Spera,DC=spera,DC=sec" `
        -SamAccountName "$_"
        
    $department | ForEach-Object {
        $localgroups = Get-ADgroup -filter * | Where-object name -eq "g_$_"      
        $localgroups | Select-Object name, samaccountname            
        $printgroup = Get-ADGroup -filter * | Where-object name -eq "print_$_"   
        $printgroup | Select-Object name, samaccountname
        Write-Host $localgroups
        Write-Host $printgroup         
        $localgroups | Add-ADGroupMember -Members $printgroup.samaccountname 
    }
}

# alle avdelingene blir lagt til i remoteaccess
$department | ForEach-Object {
    $localgroups = Get-ADgroup -filter * | Where-object name -eq "l_$_"      
    $localgroups | Select-Object name, samaccountname            
    $remotegroup = Get-ADGroup -filter * | Where-object name -eq "l_remoteaccess"   
    $remotegroup | Select-Object name, samaccountname
    Write-Host $localgroups
    Write-Host $remotegroup         
    $remotegroup | Add-ADGroupMember -Members $localgroups.samaccountname 
}

#Creates the folder "log"
mkdir C:\log
        

