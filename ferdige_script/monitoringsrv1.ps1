# Ressursene som overvåkes i srv1
$scriptblock = 
'\PhysicalDisk(_total)\Disk Transfers/sec',
'\Memory\Available MBytes',
'\Processor(_Total)\% Processor Time',
'\Process(_Total)\Working Set', 
'\Network Interface(*)\Bytes Total/sec',
'\USB(*)\Avg. Bytes/Transfer'

# Henter alle ressursene, får ut tid, path og navn på ressursen, exporterer så i csv fil
Get-Counter -Counter $scriptblock -MaxSamples 3 |
ForEach-Object {
    $_.CounterSamples | ForEach-Object {
        [PSCustomObject]@{
            TimeStamp = $_.Timestamp
            Path      = $_.Path
            Value     = $_.CookedValue
        }
    }
} | Export-CSV -Path "C:\Users\$env:username\Downloads\resourceStatus.csv" -NoTypeInformation

# Script for å overvåke IIS-nettsiden i srv1
# Dersom tjenesten har stoppet, vil det logges, tjenesten vil starte opp igjen og skrive til loggen at 
# tjenesten kjører igjen.
$thisService = Get-Service "W3SVC"
$serviceOutput = "C:\Users\$env:username\Downloads\serviceStatus.csv"
if ($thisService.status -eq "Stopped") {
    $log = "Service '" 
    $log += $thisService.Name
    $log += "' was " 
    $log += $thisService.Status
    $log += ". Time: "
    $log += (Get-Date)
    Write-Output $log >> $serviceOutput       
    try {
        Start-Service $thisService
        $log = "    Service is now "
        $log += $thisService.Status
        Write-Output $log >> $serviceOutput
    }
    catch {
        $log = "Service '"
        $log += $thisService.Name 
        $log += "' start wasnt succsesfull"
        $log += (Get-Date)
        Write-Output $log >> $serviceOutput
    }
}
else {
    $log = "Service '"
    $log += $thisService.Name 
    $log += "' is "
    $log += $thisService.Status
    $log += ". Time: "
    $log += (Get-Date)
    Write-Output $log >> $serviceOutput
}