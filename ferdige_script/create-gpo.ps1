
$GPOName = "Allow RDP"

#lage GPOen
New-GPO -name $GPOName -comment "Allows remote desktop"

#linke GPOen til de ulike departmentene
$OU = 'OU=Computers'
foreach ($item in $OU) {
    Get-GPO -Name $GPOName | New-GPLink -Target "$item,OU=Spera,DC=spera,DC=sec"
}


$GPOName = "Default home page"

# lage GPOen
New-GPO -name $GPOName -comment "Creates default start page in Internet Explorer"

# linke GPOen til de ulike departmentene
$OU = 'OU=Employees'
foreach ($item in $OU) {
    Get-GPO -Name $GPOName | New-GPLink -Target "$item,OU=Spera,DC=spera,DC=sec"
}


$GPOName = "disable cmd, pwsh and control panel"

#Make the GPO    
New-GPO -name $GPOName -comment "Disables cmd, powershell and control panelfor users"

# link the GPO to the different departments
$OU = 'OU=finance', 'OU=dev', 'OU=sales', 'OU=hr'
foreach ($item in $OU) {
    Get-GPO -Name $GPOName | New-GPLink -Target "$item,OU=Employees,OU=Spera,DC=spera,DC=sec"
}
$GPOName = "maped drives"

#Make the GPO
New-GPO -name $GPOName -comment "Enables maped drives"

# link the GPO to the different departments
$OU = 'OU=Employees'
foreach ($item in $OU) {
    Get-GPO -Name $GPOName | New-GPLink -Target "$item,OU=Spera,DC=spera,DC=sec"
}
$GPOName = "Hash"

# Make the GPO
New-GPO -name $GPOName -comment "Do not store hash valuees"

# link the GPO to the different departments
$OU = 'OU=Employees'
foreach ($item in $OU) {
    Get-GPO -Name $GPOName | New-GPLink -Target "$item,OU=Spera,DC=spera,DC=sec"
}
$GPOName = "Dont force restart"

# Make the GPO
New-GPO -name $GPOName -comment "Disables Forces Restart of Computers because Valueable Information Can Get Lost"

#  link the GPO to the different departments
$OU = 'OU=Employees'
foreach ($item in $OU) {
    Get-GPO -Name $GPOName | New-GPLink -Target "$item,OU=Spera,DC=spera,DC=sec"
}