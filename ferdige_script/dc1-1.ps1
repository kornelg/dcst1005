# Prompting for password for new user spera.sec\administrator
do {
    $Password = Read-Host -Prompt 'Enter New Password for spera.sec\administrator' -AsSecureString
    $Password2 = Read-Host -Prompt 'Repeat New Password for spera.sec\administrator' -AsSecureString
    $pwd1_text = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($Password))
    $pwd2_text = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($Password2))
} until ($pwd1_text -ceq $pwd2_text)

Install-WindowsFeature AD-Domain-Services, DNS -IncludeManagementTools

# Adding user spear.sec\administrator
Set-LocalUser -Password $Password Administrator
$Params = @{
    DomainMode = 'WinThreshold'
    DomainName = 'spera.sec'
    DomainNetbiosName = 'spera'
    ForestMode = 'WinThreshold'
    InstallDns = $true
    NoRebootOnCompletion = $true
    SafeModeAdministratorPassword = $Password
    Force = $true
}

Install-ADDSForest @Params
Restart-Computer

