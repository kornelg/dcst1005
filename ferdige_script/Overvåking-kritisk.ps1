$scriptblock = {
    $services = 'DNS', 'DFS Replication', 'Intersite Messaging', 'Kerberos Key Distribution Center', 'NetLogon', 'Active Directory Domain Services', 'DFS Namespace', 'wuauserv', 'DsRoleSvc'
    $status = Get-Service -Name $services
    $sleepTime = 10
    $retries = 3
    $logOutput = 'C:\log\log.txt'

    0..$retries | % { 
        foreach ($thisService in $status) {
            if ($thisService.status -eq 'Stopped') {
                $log = "Service '" 
                $log += $thisService.Name
                $log += "' wasnt running. Time: "
                $log += (Get-Date)
                Write-Output $log >> $logOutput         
                try {
                    Start-Service $thisService
                    $log = "    Service is now "
                    $log += $thisService.Status
                    Write-Output $log >> $logOutput
                }
                catch {
                    $log = "Service '"
                    $log += $thisService.Name 
                    $log += "' start wasnt succsesfull"
                    $log += (Get-Date)
                    Write-Output $log >> $logOutput
                }
            }
        }
        Start-Sleep -Seconds ($sleepTime)
    }
}

Invoke-Command -ComputerName dc1 `
    -ScriptBlock $scriptblock