# mgr remoted into dc1
$csv = Import-Csv csv-fil\IT-admin-users.csv

Write-Output "username,password">OversiktAdmin.csv
$existingUsers = @()
foreach ($i in $csv) {
    $skipFlag = $true
    # Sjekker om det finnes en bruker med samme for og etter -navn
    #her må vi legge til promt YES/NO
    $GivenName = $i.GivenName
    $Surname = $i.Surname
    if ("$Surname, $GivenName" -in $existingUsers) {
        $skip = Read-Host -Prompt '$Surname, $GivenName is a duplicate name, skip? [Y,n]'
        switch -Wildcard ($skip) {
            '' { continue }
            'y*' { continue }
            'n*' {}
            Default {}
        }
        $skipFlag = $false
    }
    $existingUsers += "$Surname, $GivenName"
    $filter = 'Surname -eq $Surname -and GivenName -eq $GivenName'
    if ((Get-ADUser -Filter $filter) -and (!$skipFlag)) {
        Write-Output "$Surname, $GivenName : already exist, skipping"
        continue
    }

    $symbols = '!@#$%^&*'.ToCharArray()
    #$characterList = 'a'..'z' + 'A'..'Z' + '0'..'9' + $symbols
    $characterList = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".ToCharArray() + $symbols

    function GeneratePassword {
        param(
            [ValidateRange(12, 256)]
            [int] 
            $length = 7
        )

        do {
            $password = -join (0..$length | % { $characterList | Get-Random })
            [int]$hasLowerChar = $password -cmatch '[a-z]'
            [int]$hasUpperChar = $password -cmatch '[A-Z]'
            [int]$hasDigit = $password -match '[0-9]'
            [int]$hasSymbol = $password.IndexOfAny($symbols) -ne -1

        }
        until (($hasLowerChar + $hasUpperChar + $hasDigit + $hasSymbol) -ge 3)

        # $password | ConvertTo-SecureString -AsPlainText -Force
        return [string]$password
    }

    $domain = "spera.sec"

    #sjekk om username finst i AD, hvis nei count +1 fram til en ikkje får treff i AD
    $count = 0

    $mGivenName = $i.GivenName
    $mSurname = $i.Surname
    while ($mGivenName.Length -lt 3) {
        $mGivenName += $mGivenName[$mGivenName.Length - 1]
    }
    while ($mSurname.Length -lt 3) {
        $mSurname += $mSurname[$mSurname.Length - 1]
    }
    do {
        $username = ("a" + $mSurname.substring(0, 3) + $mGivenName.substring(0, 3) + $count.ToString('00')).ToLower()
        $count += 1
    } until ($null -eq (Get-ADUser -Filter * | Where-Object SamAccountName -eq $username))
    $UserPrincipalName = $username + "@" + $domain
    $DisplayName = $i.GivenName + " " + $i.Surname + $(if ($count -eq 1) { "" } else { $count }) + "[ADM]"
    $password = (GeneratePassword)
    $plain = $password
    $password = $password |  ConvertTo-SecureString -AsPlainText -Force
    
    New-ADUser -Name $DisplayName `
        -GivenName $i.GivenName `
        -Surname $i.Surname `
        -SamAccountName  $username `
        -UserPrincipalName  $UserPrincipalName `
        -Path "OU=$($i.Department),OU=Employees,OU=Spera,DC=spera,DC=sec" `
        -Department $i.Department `
        -AccountPassword $password `
        -Enabled $true `
        -ChangePasswordAtLogon $true

    Write-Output $username
    Write-Output "$username,$plain">>OversiktAdmin.csv
}

function ADGroup {
    param (
        $OU
    )
    $users = Get-ADUser -Filter * -SearchBase "OU=$OU,OU=Employees,OU=Spera,DC=spera,DC=sec"
    if ($users) {
        Add-ADGroupMember -Identity "g_$OU" -Members $users
        Add-ADGroupMember -Identity "l_$OU" -Members $users

        if ($OU -eq "IT") {
            Add-ADGroupMember -Identity "Domain Admins" -Members $users
        }

    }
}

#Users gets AD-groups based on which OU they're a part of
ADGroup("dev")
ADGroup("finance")
ADGroup("sales")
ADGroup("IT")
ADGroup("HR")

Write-Output "Deleting CSV file"
pause
Write-Output "" > .\csv-fil\IT-admin-users.csv