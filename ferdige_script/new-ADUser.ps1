# mgr remoted into dc1

$csv = Import-Csv csv-fil\user.csv
Write-Output "username,password" > Oversikt.csv
$existingUsers = @()

# Defining the symbols which will be used for the password-function
$symbols = '!@#$%^&*'.ToCharArray()
$characterList = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".ToCharArray() + $symbols
# Picks a random symbol 8 times, until the text-string meets 3 out of 4 of the password requirements
function GeneratePassword {
    param(
        [ValidateRange(12, 256)]
        [int] 
        $length = 7
    )

    do {
        $password = -join (0..$length | ForEach-Object { $characterList | Get-Random })
        [int]$hasLowerChar = $password -cmatch '[a-z]'
        [int]$hasUpperChar = $password -cmatch '[A-Z]'
        [int]$hasDigit = $password -match '[0-9]'
        [int]$hasSymbol = $password.IndexOfAny($symbols) -ne -1

    }
    until (($hasLowerChar + $hasUpperChar + $hasDigit + $hasSymbol) -ge 3)
    return [string]$password
}
function ADGroup {
    param (
        $OU
    )
    $users = Get-ADUser -Filter * -SearchBase "OU=$OU,OU=Employees,OU=Spera,DC=spera,DC=sec"
    if ($users) {
        Add-ADGroupMember -Identity "g_$OU" -Members $users
        Add-ADGroupMember -Identity "l_$OU" -Members $users
        Add-ADGroupMember -Identity "print_$OU" -Members $users
    }
}

:mainLoop foreach ($i in $csv) {
    # Checks if there is a user with a similar given name and surname
    $GivenName = $i.GivenName
    $Surname = $i.Surname
    
    $existingUsers += "$Surname, $GivenName"
    $filter = 'Surname -eq $Surname -and GivenName -eq $GivenName'
    if (Get-ADUser -Filter $filter) {
        :innerLoop while ($true) {   
            $skip = Read-Host -Prompt "$Surname, $GivenName is a duplicate name, skip? [Y/n]"
            switch -Wildcard ($skip) {
                '' { 
                    Write-Output "skipping $Surname, $GivenName"
                    continue mainLoop
                }
                'y*' {     
                    Write-Output "skipping $Surname, $GivenName"
                    continue mainLoop
                }
                'n*' { break innerLoop }
            }
        }
    }

    $domain = "spera.sec"

    # check if username exists in AD, if not count +1 until no matches are found in AD
    $count = 0

    $mGivenName = $i.GivenName
    $mSurname = $i.Surname
    while ($mGivenName.Length -lt 3) {
        $mGivenName += $mGivenName[$mGivenName.Length - 1]
    }
    while ($mSurname.Length -lt 3) {
        $mSurname += $mSurname[$mSurname.Length - 1]
    }
    do {
        $username = ("u" + $mSurname.substring(0, 3) + $mGivenName.substring(0, 3) + $count.ToString('00')).ToLower()
        $count += 1
    } until (!( Get-ADUser -Filter * | Where-Object SamAccountName -eq $username ))
    
    # Variables for ADUser
    $UserPrincipalName = $i.GivenName + "." + $i.Surname + $(if ($count -eq 1) { "" } else { $count }) + "@" + $domain
    $DisplayName = $i.GivenName + " " + $i.Surname + $(if ($count -eq 1) { "" } else { $count })
    $password = (GeneratePassword)
    $plain = $password
    $password = $password |  ConvertTo-SecureString -AsPlainText -Force
    
    New-ADUser -Name $DisplayName `
        -GivenName $i.GivenName `
        -Surname $i.Surname `
        -SamAccountName  $username `
        -UserPrincipalName  $UserPrincipalName `
        -Path "OU=$($i.Department),OU=Employees,OU=Spera,DC=spera,DC=sec" `
        -Department $i.Department `
        -AccountPassword $password `
        -Enabled $true `
        -ChangePasswordAtLogon $true

    Write-Output $username
    Write-Output "$username,$plain" >> Oversikt.csv

}

# Users are assigned to AD-groups based on which OU they are in
ADGroup("dev")
ADGroup("finance")
ADGroup("sales")
ADGroup("IT")
ADGroup("HR")


Write-Output "Deleting CSV file"
pause
# Clearing user.csv so it can be reused
Write-Output "" > csv-fil\user.csv
